<?php include ('cabecalho.php');
$bg_contato ='background: url(img/equipe/bg-equipe.jpg) no-repeat center center fixed;-webkit-background-size: cover;-moz-background-size: cover;background-size: cover;-o-background-size: cover;';
?>
<body>
<!-- Header -->
<header id="top" class="header2" style="<?php echo $bg_contato?>">
<div class="text-vertical-center">
<h1 class="titles-text-branco">Nossa Equipe</h1>
<br>
<!--<a href="#post" class="btn btn-dark btn-lg">Veja Mais</a>-->
</div>
</header>
<?php include ('./navbar.php'); ?>
<!-- Post -->
<section id="post" class="post">
<div class="container">
<div class="row">
<div class="container text-center">
<hr class="small">
<div class="row wow fadeInUp animated" data-wow-delay="0,5s" data-wow-duration="0,5s"> 
<!----------------------------------------------------------------------------->
<div class="col-lg-4 col-md-4 col-sm-4">
<img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
<h2>Heading</h2>
<p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
<p><a class="btn btn-secondary" href="<?php // echo $this->asset ?>#" role="button">View details &raquo;</a></p>
</div><!-- /.col-lg-4 -->
<!----------------------------------------------------------------------------->
<div class="col-lg-4 col-md-4 col-sm-4">
<img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
<h2>Heading</h2>
<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
<p><a class="btn btn-secondary" href="<?php // echo $this->asset ?>#" role="button">View details &raquo;</a></p>
</div><!-- /.col-lg-4 -->
<!----------------------------------------------------------------------------->
<div class="col-lg-4 col-md-4 col-sm-4">
<img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
<h2>Heading</h2>
<p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
<p><a class="btn btn-secondary" href="<?php // echo $this->asset ?>#" role="button">View details &raquo;</a></p>
</div><!-- /.col-lg-4 -->
<!----------------------------------------------------------------------------->
<div class="col-lg-4 col-md-4 col-sm-4">
<img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
<h2>Heading</h2>
<p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
<p><a class="btn btn-secondary" href="<?php // echo $this->asset ?>#" role="button">View details &raquo;</a></p>
</div><!-- /.col-lg-4 -->
<!----------------------------------------------------------------------------->
<div class="col-lg-4 col-md-4 col-sm-4">
<img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
<h2>Heading</h2>
<p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
<p><a class="btn btn-secondary" href="<?php // echo $this->asset ?>#" role="button">View details &raquo;</a></p>
</div><!-- /.col-lg-4 -->
<!----------------------------------------------------------------------------->
<div class="col-lg-4 col-md-4 col-sm-4">
<img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
<h2>Heading</h2>
<p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
<p><a class="btn btn-secondary" href="<?php // echo $this->asset ?>#" role="button">View details &raquo;</a></p>
</div><!-- /.col-lg-4 -->
<!----------------------------------------------------------------------------->
</div>
<!-- /.row -->
</div>
</div>
<!-- /.row -->
</div>
<!-- /.container -->
</section>
<?php include ('./rodape.php'); ?>