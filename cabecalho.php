<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">
<title>Ambiensense</title>
<!-- Bootstrap Core CSS -->
<link href="<?php // echo $this->asset ?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php // echo $this->asset  ?>css/components.css" rel="stylesheet" type="text/css"/>
<!-- Custom CSS -->
<link href="<?php // echo $this->asset ?>css/camera.css" rel="stylesheet" type="text/css"/>
<link href="<?php // echo $this->asset ?>css/stylish-portfolio.css" rel="stylesheet">
<!-- Custom Fonts -->
<link href="<?php // echo $this->asset ?>font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php // echo $this->asset ?>css/animate.css" rel="stylesheet" type="text/css"/>
<script src="<?php // echo $this->asset ?>js/jquery.js" type="text/javascript"></script>
<script src="<?php // echo $this->asset ?>js/wow.js" type="text/javascript"></script>
<script src="<?php // echo $this->asset ?>js/jquery-migrate-1.2.1.js"></script>
<script src="js/jquery.rd-parallax.js" type="text/javascript"></script>
<script src="js/modernizr.custom.js" type="text/javascript"></script>
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
<link rel="icon" href="img/favicon.svg"  />
<!--[if IE]><link rel="shortcut icon" href="img/favicon.svg"><![endif]-->
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
$('.collapse').on('shown.bs.collapse', function(){
$(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
}).on('hidden.bs.collapse', function(){
$(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
});        
</script>
<script>
new WOW().init();
</script>
</head>