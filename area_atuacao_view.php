<?php include ('cabecalho.php');
$bg_a_atuacao ='background: url(img/index/bg-area-atuacao.jpg) no-repeat fixed;-webkit-background-size: cover;-moz-background-size: cover;background-size: cover;-o-background-size: cover;';
?>
<body>
<!-- Header -->
<header id="top" class="header2" style="<?php echo $bg_a_atuacao?>">
<div class="text-vertical-center">
<h1 class="titles-text-branco">Áreas de Atuação</h1>
<br>
</div>
</header>
<?php include ('./navbar.php'); ?>
<!-- About -->
<section id="post" class="post">
<div class="container">
<div class="row">
<div class="col-lg-12 text-justify">
<div class="panel-group" id="accordion">
<div class="panel panel-default">
<div class="panel-heading">
    
<a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
<h4 class="panel-title">
Collapsible Group 1 
<i class="link1 pull-right">
<i class="fa fa-chevron-down"></i>
</i>
</h4>
</a>
</div>
<div id="collapse1" class="panel-collapse collapse collapseBS">
<div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
commodo consequat.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
<h4 class="panel-title">
Collapsible Group 2
<i class="link2 pull-right">
<i class="fa fa-chevron-down"></i>
</i>
</h4>
</a>
</div>
<div id="collapse2" class="panel-collapse collapse collapseBS">
<div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
commodo consequat.</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
Collapsible Group 3</a>
</h4>
</div>
<div id="collapse3" class="panel-collapse collapse">
<div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
commodo consequat.</div>
</div>
</div>
</div>
</div>
</div>
<!-- /.row -->
</div>
<!-- /.container -->
</section>
<?php include ('./rodape.php'); ?>