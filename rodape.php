<div class="top-footer"></div>
<footer class="rodape container-fluid" >
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-3 wow animated bounceIn" data-wow-delay="0.8s" >
<ul class="list-unstyled">
<li><span class="fa fa-home"></span><strong> Ambiensense    </strong></li>
<li><a href="<?php // echo $this->asset ?>quemsomos.php" class="text-branco"> Quem Somos</a></li>
<li><a href="<?php // echo $this->asset ?>equipe.php" class="text-branco"> A Equipe</a></li>
</ul>
</div><!-- /.col-lg-3 col-md-3 col-sm-3 -->
<div class="col-lg-3 col-md-3 col-sm-3 wow animated bounceIn space-top-sm-2" data-wow-delay="0.9s">
<ul class="list-unstyled">
<li><span class="fa fa-handshake-o"></span><strong> Serviços</strong></li>
<li><a href="<?php // echo $this->asset ?>#" class="text-branco"> Avaliações e pareceres</a></li>
<li><a href="<?php // echo $this->asset ?>#" class="text-branco"> Assessoria</a></li>
<li><a href="<?php // echo $this->asset ?>#" class="text-branco"> Consultoria</a></li>
<li><a href="<?php // echo $this->asset ?>#" class="text-branco"> Execução de projetos</a></li>
<li><a href="<?php // echo $this->asset ?>#" class="text-branco"> Observação, investigação e monitoramento</a></li>
<li><a href="<?php // echo $this->asset ?>#" class="text-branco"> Auditorias, inspeções e perícias</a></li>
<li><a href="<?php // echo $this->asset ?>#" class="text-branco"> Treinamentos, cursos e palestras</a></li>
</ul>
</div><!-- /.col-lg-3 col-md-3 col-sm-3 -->
<div class="col-lg-3 col-md-3 col-sm-3 wow animated bounceIn space-top-sm-2" data-wow-delay="1s">
<ul class="list-unstyled">
<li><i class="fa fa-share-alt"></i><strong> Redes Sociais</strong>
<br/><span>Encontre-nos nas redes sociais. </span> <br/>
</li>
<li>
<i class="fa fa-linkedin-square"></i>
<a href="#"target="_blank" class="text-branco">
Linkedin
</a>
<li>
<i class="fa fa-facebook-square"></i>
<a href="#"target="_blank" class="text-branco">
Facebook
</a>
</li>
<li>
<i class="fa fa-twitter-square"></i>
<a href="#" target="_blank" class="text-branco">
@ambiensense
</a>
</li>
<li>
<i class="fa fa-envelope-square"></i>
<a class="text-branco">
contato@ambiensense.com.br
</a>
</li>
</ul>
</div><!-- /.col-lg-3 col-md-3 col-sm-3 -->
<div class="col-lg-3 col-md-3 col-sm-3 wow animated bounceIn space-top-sm-2" data-wow-delay="1.1s">
<ul class="list-unstyled">
<li>
<strong><i class="glyphicon glyphicon-map-marker"></i> Localização</strong>
</li>
<li>
R. Alm. Barroso, 751 - Centro
Santana do Livramento - RS - Brasil
</li>
<li>
<span class="glyphicon glyphicon-phone-alt "> </span>
<strong> (xx) xxxx xxxx </strong><br/>
</li>
<li>
<span class="glyphicon glyphicon-phone"> </span>
<strong> (xx) x xxxx xxxx </strong><br/>
</li>
<li>
<i class="fa fa-whatsapp"></i>
<strong>&nbsp;(xx) x xxxx xxxx </strong><br/>
</li>
</ul>
</div><!-- /.col-lg-3 col-md-3 col-sm-3 -->
</div>
<div class="navbar navbar-default" style="background: rgba(51, 51, 51, 0);">
<div class="container-fluid">
<div class="row">
<div class="navbar-text" style="color: #fff; font-size: 80%; position: relative; ">
Copyright © <?php echo date('Y'); ?><span itemprop="url"> ambiensense.com.br</span> Todos os direitos reservados. Design by <span class="wow bounceInRight animated">Ambiensense</span>
</div>
</div>
</div>
</div>
</div>
</footer>
<!-- jQuery -->
<!-- Bootstrap Core JavaScript -->
<script src="<?php // echo $this->asset ?>js/bootstrap.min.js"></script>
<script src="<?php // echo $this->asset ?>js/toucheffects.js" type="text/javascript"></script>
<!-- Custom Theme JavaScript -->
<script>
// Closes the sidebar menu
$("#menu-close").click(function(e) {
e.preventDefault();
$("#sidebar-wrapper").toggleClass("active");
});
// Opens the sidebar menu
$("#menu-toggle").click(function(e) {
e.preventDefault();
$("#sidebar-wrapper").toggleClass("active");
});
// Scrolls to the selected menu item on the page
$(function() {
$('a[href*=#]:not([href=#],[data-toggle],[data-target],[data-slide])').click(function() {
if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {
var target = $(this.hash);
target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
if (target.length) {
$('html,body').animate({
scrollTop: target.offset().top
}, 1000);
return false;
}
}
});
});
//#to-top button appears after scrolling
var fixed = false;
$(document).scroll(function() {
if ($(this).scrollTop() > 250) {
if (!fixed) {
fixed = true;
// $('#to-top').css({position:'fixed', display:'block'});
$('#to-top').show("slow", function() {
$('#to-top').css({
position: 'fixed',
display: 'block'
});
});
}
} else {
if (fixed) {
fixed = false;
$('#to-top').hide("slow", function() {
$('#to-top').css({
display: 'none'
});
});
}
}
});
// Disable Google Maps scrolling
// See http://stackoverflow.com/a/25904582/1607849
// Disable scroll zooming and bind back the click event
var onMapMouseleaveHandler = function(event) {
var that = $(this);
that.on('click', onMapClickHandler);
that.off('mouseleave', onMapMouseleaveHandler);
that.find('iframe').css("pointer-events", "none");
}
var onMapClickHandler = function(event) {
var that = $(this);
// Disable the click handler until the user leaves the map area
that.off('click', onMapClickHandler);
// Enable scrolling zoom
that.find('iframe').css("pointer-events", "auto");
// Handle the mouse leave event
that.on('mouseleave', onMapMouseleaveHandler);
}
// Enable map zooming with mouse scroll when the user clicks the map
$('.map').on('click', onMapClickHandler);

$(document).ready(function(){
$("#collapse1").on("hide.bs.collapse", function(){
$(".link1").html('<span class="fa fa-chevron-down"></span>');
});
$("#collapse1").on("show.bs.collapse", function(){
$(".link1").html('<span class="fa fa-minus"></span>');
});
});
/*#####################
Additional jQuery (required)
#####################*/
$(document).ready(function(){
var clickEvent = false;
$('#myCarousel').carousel({
interval:   4000
}).on('click', '.list-group li', function() {
clickEvent = true;
$('.list-group li').removeClass('active');
$(this).addClass('active');
}).on('slid.bs.carousel', function(e) {
if(!clickEvent) {
var count = $('.list-group').children().length -1;
var current = $('.list-group li.active');
current.removeClass('active').next().addClass('active');
var id = parseInt(current.data('slide-to'));
if(count == id) {
$('.list-group li').first().addClass('active');
}
}
clickEvent = false;
});
})
$(function(){
$('a[title]').tooltip();
});

$(window).load(function() {
var boxheight = $('#myCarousel .carousel-inner').innerHeight();
var itemlength = $('#myCarousel .item').length;
var triggerheight = Math.round(boxheight/itemlength+1);
$('#myCarousel .list-group-item').outerHeight(triggerheight);
});
</script>
<!-- Start of Rocket.Chat Livechat Script -->
<script type="text/javascript">
(function(w, d, s, u) {
w.RocketChat = function(c) { w.RocketChat._.push(c) }; w.RocketChat._ = []; w.RocketChat.url = u;
var h = d.getElementsByTagName(s)[0], j = d.createElement(s);
j.async = true; j.src = 'https://ambiensense.rocket.chat/packages/rocketchat_livechat/assets/rocketchat-livechat.min.js?_=201702160944';
h.parentNode.insertBefore(j, h);
})(window, document, 'script', 'https://ambiensense.rocket.chat/livechat');
</script>
<!-- End of Rocket.Chat Livechat Script -->
<div class="clearfix"></div>
</body>
</html>