<?php include ('./cabecalho.php');
$bg_news ='background: url(img/img-cabecalho-news.jpg) no-repeat top center fixed;-webkit-background-size: cover;-moz-background-size: cover;background-size: cover;-o-background-size: cover;';
?>
<body>
<!-- Header -->
<header id="top" class="header2" style="<?php echo $bg_news?>">
<div class="text-vertical-center">
<h1 class="titles-text-branco">Blog</h1>
<br>
<!--<a href="#post" class="btn btn-dark btn-lg">Veja Mais</a>-->
</div>
</header>
<?php include ('./navbar.php'); ?>
<!-- Post -->
<section id="post" class="mx-w-m-a">
<div class="container">

<div class="row">
<section id="todas-noticias">
<!--NOTICIAS DA // ESQUERDA-->
<!----------------------------------------------------------------------------->
<div class="col-md-8">
<!--1ª NOTICIA, EM DESTAQUE´ // ESQUERDA-->
<!----------------------------------------------------------------------------->
<article class="noticia noticia--primera" itemscope="" itemtype="https://schema.org/NewsArticle" itemref="organizacion" data-vr-contentbox="">
<div class="conteudo-noticia">
<!--IMAGEM DA 1ª NOTICIA, EM DESTAQUE // ESQUERDA-->
<figure class="foto" itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
<a href="<?php // echo $this->asset ?>#">
<img class="img-responsive" src="<?php // echo $this->asset ?>http://placehold.it/750x450/dddddd/333333" alt="Descrição da Imagem" >
<meta itemprop="url" content="urldaimagem">
</a>
<figcaption class="foto-caption" itemprop="caption">
<span class="foto-texto">Curabitur aliquet quam id dui posuere blandit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
<span class="foto-assinatura">
<span itemprop="author" class="foto-autor">Nome do autor</span>.
<span itemprop="copyrightHolder" class="foto-agencia">AMBIENSENSE</span>
</span>
</figcaption>
</figure>
<!--FIM IMAGEM DA 1ª NOTICIA, EM DESTAQUE // ESQUERDA-->
<!----------------------------------------------------------------------------->
<!--TITULO DA 1ª NOTICIA, EM DESTAQUE // ESQUERDA-->
<!----------------------------------------------------------------------------->
<h2 itemprop="headline" class="noticia-titulo">
<a href="<?php // echo $this->asset ?>#">Quisque velit nisi, pretium ut lacinia in, elementum id enim. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.</a>
</h2>
<!--DADOS DA 1ª NOTICIA, EM DESTAQUE // ESQUERDA-->
<!----------------------------------------------------------------------------->
<div class="noticia-metadatos">
<div class="assinatura">
<div class="autor" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
<div class="autor-texto">
<span class="autor-nome" itemprop="name">
<span>Nome do Autor</span>
</div>
</div>
</div>
<div class="noticia-datos">
<span class="noticia-data">
dd/mm/aaaa
</span>
</div>
</div>
<!--SUBTITULO DA 1ª NOTICIA, EM DESTAQUE // ESQUERDA-->
<!----------------------------------------------------------------------------->
<p class="subtitulo-noticia" itemprop="description">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Proin eget tortor risus. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>
<meta content="2017-06-10T16:46:00+02:00" itemprop="datePublished">
<meta content="2017-06-10T16:46:00+02:00" itemprop="dateModified">
</div>
</article>
<!--FIM 1ª NOTICIA, EM DESTAQUE // ESQUERDA-->
<!----------------------------------------------------------------------------->
<!--2ª NOTICIA, EM DESTAQUE EQUERDA-->
<article class="noticia" itemscope="" itemtype="https://schema.org/NewsArticle" itemref="organizacion" data-vr-contentbox="">
<div class="conteudo-noticia">
<span class="oculto" itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
<meta itemprop="url" content="urldaimagem">
</span>
<h2 itemprop="headline" class="noticia-titulo">
<a href="<?php // echo $this->asset ?>#">Pellentesque in ipsum id orci porta dapibus.</a>
</h2>
<!--DADOS 2ª NOTICIA // ESQUERDA-->
<!----------------------------------------------------------------------------->
<div class="noticia-metadatos">
<div class="assinatura">
<div class="autor" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
<div class="autor-texto">
<span class="autor-nome" itemprop="name">
<span>Nome do Autor</span>
</span>
</div>
</div>
</div>
<div class="noticia-datos">
<span class="noticia-data">
dd/mm/aaaa
</span>
</div>
<!--SUBTITULO 2ª NOTICIA // ESQUERDA-->
<!----------------------------------------------------------------------------->
<p class="subtitulo-noticia" itemprop="description">Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.</p>
<meta content="2017-06-10T16:51:00+02:00" itemprop="datePublished">
<meta content="2017-06-10T16:51:00+02:00" itemprop="dateModified">
</div>
</div>
</article>
</div>
<!--FIM 2ª NOTICIA, EM DESTAQUE EQUERDA-->
<!----------------------------------------------------------------------------->
<!--FIM NOTICIAS DA // ESQUERDA-->
<!----------------------------------------------------------------------------->
<!--NOTICIAS DA // DIREITA-->
<!----------------------------------------------------------------------------->
<div class="col-md-4">
<!--3ª NOTICIA DA // DIREITA-->
<!----------------------------------------------------------------------------->
<article class="noticia" itemscope="" itemtype="https://schema.org/NewsArticle" itemref="organizacion" data-vr-contentbox="">
<div class="conteudo-noticia">
<!--IMAGEM DA 1ª NOTICIA DA // DIREITA-->
<figure class="foto foto_w640" itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
<a href="<?php // echo $this->asset ?>#">
<img class="img-responsive" src="<?php // echo $this->asset ?>http://placehold.it/360x180/dddddd/333333" alt="Descrição da Imagem" >
<meta itemprop="url" content=urldaimagem">
</a>
<figcaption class="foto-caption" itemprop="caption">
<span class="foto-texto">Nulla porttitor accumsan tincidunt.</span>
<span class="foto-assinatura"> <span itemprop="copyrightHolder" class="foto-agencia">AMBIENSENSE</span></span>
</figcaption>
</figure>
<!--TITULO DA 3ª NOTICIA DA // DIREITA-->
<!----------------------------------------------------------------------------->
<h2 itemprop="headline" class="noticia-titulo">
<a href="<?php // echo $this->asset ?>#">Curabitur aliquet quam id dui posuere blandit.</a>
</h2>
<!--DADOS DA 1ª NOTICIA // DIREITA-->
<!----------------------------------------------------------------------------->
<div class="noticia-metadatos">
<div class="assinatura">
<div class="autor" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
<div class="autor-texto">
<span class="autor-nome" itemprop="name">
<span>Nome do Autor</span>
</span>
</div>
</div>
</div>
<div class="noticia-datos">
<span class="noticia-data">
dd/mm/aaaa
</span>
</div>
<!--SUBTITULO DA 3ª NOTICIA // DIREITA-->
<p class="subtitulo-noticia" itemprop="description">Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit.</p>
<meta content="2017-06-10T17:06:00+02:00" itemprop="datePublished">
<meta content="2017-06-10T17:06:00+02:00" itemprop="dateModified">
</div>
</div>
</article>
<!--FIM DA 1ª NOTICIA // DIREITA-->
<!----------------------------------------------------------------------------->
<!--2ª NOTICIA // DIREITA-->
<!----------------------------------------------------------------------------->
<article class="noticia" itemscope="" itemtype="https://schema.org/NewsArticle" itemref="organizacion" data-vr-contentbox="">
<div class="conteudo-noticia">
<span class="oculto" itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
<meta itemprop="url" content="urldaimagem">
<meta itemprop="width" content="360">
<meta itemprop="height" content="257">
</span>
<!--TITULO 4ª NOTICIA // DIREITA-->
<!----------------------------------------------------------------------------->
<h2 itemprop="headline" class="noticia-titulo">
<a href="<?php // echo $this->asset?>#">Proin eget tortor risus. Donec sollicitudin molestie malesuada.</a>
</h2>
<!--DADOS 4ª NOTICIA // DIREITA-->
<!----------------------------------------------------------------------------->
<div class="noticia-metadatos">
<div class="assinatura">
<div class="autor" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
<div class="autor-texto">
<span class="autor-nome" itemprop="name">
<span>Nome do Autor</span>
</span>
</div>
</div>
</div>
<div class="noticia-datos">
<span class="noticia-data">
dd/mm/aaaa
</span>
</div>
<!--SUBTITULO 4ª NOTICIA // DIREITA-->
<!----------------------------------------------------------------------------->
<p class="subtitulo-noticia" itemprop="description">Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.</p>
<meta content="2017-06-10T16:51:00+02:00" itemprop="datePublished">
<meta content="2017-06-10T16:51:00+02:00" itemprop="dateModified">
</div>
</div>
</article>
<!--FIM 4ª NOTICIA // DIREITA-->
<!----------------------------------------------------------------------------->
<!--5ª NOTICIA // DIREITA-->
<!----------------------------------------------------------------------------->
<article class="noticia" itemscope="" itemtype="https://schema.org/NewsArticle" itemref="organizacion" data-vr-contentbox="">
<div class="conteudo-noticia">
<span class="oculto" itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
<meta itemprop="url" content="urldaimagem">
<meta itemprop="width" content="360">
<meta itemprop="height" content="257">
</span>
<!--TITULO 3ª NOTICIA // DIREITA-->
<!----------------------------------------------------------------------------->
<h2 itemprop="headline" class="noticia-titulo">
<a href="<?php // echo $this->asset?>#">Cras ultricies ligula sed magna dictum porta.</a>
</h2>
<!--DADOS 5ª NOTICIA // DIREITA-->
<!----------------------------------------------------------------------------->
<div class="noticia-metadatos">
<div class="assinatura">
<div class="autor" itemprop="author" itemscope="" itemtype="https://schema.org/Person">
<div class="autor-texto">
<span class="autor-nome" itemprop="name">
<span>Nome do Autor</span>
</span>
</div>
</div>
</div>
<div class="noticia-datos">
<span class="noticia-data">
dd/mm/aaaa
</span>
</div>
<!--SUBTITULO 5ª NOTICIA // DIREITA-->
<!----------------------------------------------------------------------------->
<p class="subtitulo-noticia" itemprop="description">Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Nulla quis lorem ut libero malesuada feugiat.</p>
<meta content="2017-06-10T16:51:00+02:00" itemprop="datePublished">
<meta content="2017-06-10T16:51:00+02:00" itemprop="dateModified">
</div>
</div>
</article>
<!--FIM 5ª NOTICIA // DIREITA-->
<!----------------------------------------------------------------------------->
</div>
<!--FIM NOTICIAS // DIREITA-->
<!----------------------------------------------------------------------------->
</div>
</div>
</section>
<!--<div class="container">

<div class="row">
<?php // foreach ($data['news'] as $news): ?>
<a href="<?php // echo $this->base_url ?>News/view_news/<?php // echo $news->getId_news() ?>">
<h2 class="post-title">
titulo -> <?php // echo $news->getTitulo() ?>
</h2>
</a>
<p></p>
subtitulo - ><?php // echo $news->getSub() ?>
<br>
<img src="<?php // echo $this->base_url?>system/upload/<?php // echo $news->getSrc()?>" width="50"/>
<hr>
<?php // endforeach ?>
</div>
</div>-->
<?php include ('./rodape.php'); ?>