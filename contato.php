<?php include ('cabecalho.php');
$bg_contato ='background: url(img/contato.png) no-repeat center center fixed;-webkit-background-size: cover;-moz-background-size: cover;background-size: cover;-o-background-size: cover;';
?>
<body>
<!-- Header -->
<body>
<!-- Header -->
<header id="top" class="header2" style="<?php echo $bg_contato?>">
<div class="text-vertical-center">
<h1 class="titles-text-branco">Fale Conosco</h1>
<br>
</div>
</header>
<?php include ('./navbar.php'); ?>
<!-- Post -->

<section id="post" class="post" >
<div class="container">
<div class="row">
<div class="col-lg-12 text-justify">`
<div class="panel panel-default ">
<div class="panel panel-heading text-center ">
<i class="text-success fa fa-list-alt "></i>
Formulário para contato
</div>
<div class="panel panel-body">

<form method="post" id="contato" enctype="multipart/form-data" autocomplete="on" action="obrigado.php" >
<div class="form-group">
<label for="nome-form"><i class="text-success glyphicon glyphicon-user"></i> Nome:</label>
<input type="text" id="nome-form" name="nome" required="" class="form-control" id="nome-form" placeholder="Seu nome">
</div>
<div class="form-group">
<label for="email-form"><i class="text-success fa fa-at "></i> E-mail:</label>
<input type="email2" id="email-form" name="email2" required="" class="form-control" id="email-form" placeholder="email@provedor.com">
</div>
<div class="form-group">
<label for="fone-form"><i class="text-success fa fa-phone "></i> Telefone:</label>
<input type="text" id="fone-form" name="fone" required="" class="form-control" id="fone-form" placeholder="Seu número do telefone">
<input type="hidden"name="assunto" required="" value="Dúvidas/Observações, enviada através do formulário de Contato do site.">
<input type="hidden"name="email" value="">
</div>

<div class="form-group">
<label for="msg-form"><i class="text-success fa fa-comments "></i> Mensagem:</label>
<textarea name="mensagem" required="" class="form-control" id="msg-form" placeholder="Escreva sua mensagem "></textarea>
</div>

<p style="font-size: 12px;">
<i class="fa fa-asterisk" style="color: red; "></i> É necessário preencher todos campos.
</p>
<br/>
<button type="reset" class="btn btn-default">
<i class="text-success fa fa-eraser"></i> Limpar
</button>
<button type="submit" class="btn btn-default">
<i class="text-primary glyphicon glyphicon-send"></i> Enviar
</button>
</form>

</div>
</div>
</div>
</div>
<!-- /.row -->
</div>
<!-- /.container -->
</section>
<section id="contact" >
<div class="container">
<div class="row text-center">
<div class="col-lg-12">
    <h2 style="color: #5e5e5e; font-weight: 700">Quer receber nossos conteudos?</h2>
<hr class="small">
<div class="col-lg-4 col-md-4 col-sm-4"></div>
<div class="col-lg-4 col-md-4 col-sm-4">
<form class="form-search">
<div class="input-group">
<input type="email" class="form-control " placeholder="digite seu e-mail">
<span class="input-group-btn">
<button type="submit" class="btn btn-search">Enviar</button>
</span>
</div>
</form>
</div>
<div class="col-lg-4 col-md-4 col-sm-4"></div>
</div>
</div>
</div>
</section>
<?php include ('./rodape.php'); ?>