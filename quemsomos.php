<?php
include ('cabecalho.php');
$bg_a_atuacao = 'background: url(img/index/bg-area-atuacao.png) no-repeat center center scroll;-webkit-background-size: cover;-moz-background-size: cover;background-size: cover;-o-background-size: cover;';
?>
<body>
<!-- Header -->
<header id="top" class="header" style="<?php echo $bg_a_atuacao ?>">
<div class="text-vertical-center">
<h1>Áreas de Atuação</h1>
<h3>Geomática e topografia</h3>
<br>
<a href="#post" class="btn btn-dark btn-lg">Veja Mais</a>
</div>
</header>
<nav class="navbar navbar-default m-0 navbar-fixed-top">
<div class="container-fluid">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-toggle navbar-left" style="float: left; margin-left: 15px; padding: 0px; border: 0px;" href="index.php"><img src="img/logo_34px.png" class="img-responsive" alt="logo" /></a>
</div>
<nav class="collapse navbar-collapse" id="myNavbar">
<a style="float: left; margin-top: 8px; margin-left: 15px; padding: 0px; border: 0px;" href="index.php"><img src="img/logo_34px.png" class="img-responsive" alt="logo" /></a>
<?php include ('./navbar.php'); ?>
</nav>
</div>
</nav>
<!-- Post -->
<section id="post" class="post">
<div class="container">
<div class="row"></div>
<!-- /.row -->
</div>
<!-- /.container -->
</section>
<?php include ('./rodape.php'); ?>