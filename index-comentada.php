<?php include ('./cabecalho.php'); ?>
<body class="mx-w-m-a">
<!-- Header -->
<header id="top" >
<div id="particles-js" class="row cabecalho-index m-0" >
<!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-2"></div>-->
<div class="position-r" style="width: 100%;">
<!--------------------------------------------------------------->
<!--AQUI TEM QUE ARRUMAR O LINK PARA BASE-->
<!--------------------------------------------------------------->
<a href="<?php echo $this->asset ?>index.php">
<img src="<?php echo $this->asset ?>img/logo.svg" class="img-responsive position-a m-auto logo wow jackInTheBox" style="margin-top: 16px;" alt="Logo" />
</a>
</div>
<!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-2"></div>-->
<script src="<?php echo $this->asset ?>js/particles.js" type="text/javascript"></script>
<script src="<?php echo $this->asset ?>js/app.js" type="text/javascript"></script>
</div>
<!-- Navigation -->
<!----------------------------------------------------------------------------->
<div style="height: 50px">
<nav class="navbar navbar-default m-0 " data-spy="affix" data-offset-top="151" >
<div class="container-fluid">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a href="<?php echo $this->asset ?>index.php" class="navbar-toggle navbar-left index w-34" style="float: left; margin-left: 15px; padding: 0px; border: 0px;">
<img src="<?php echo $this->asset ?>img/logo_redondo.svg" class="img-responsive" alt="logo" /></a>
</div>
<nav class="collapse navbar-collapse mx-w-m-a" id="myNavbar">
<a href="<?php echo $this->asset ?>index.php" class="hidden-xs index w-34" style="float: left; margin-top: 8px; margin-left: 15px; padding: 0px; border: 0px;">
<img src="<?php echo $this->asset ?>img/logo_redondo.svg" class="img-responsive" alt="logo" /></a>
<ul class="nav navbar-nav text-center-sm ul-navbar float-r-lg float-r-md" >
<li><a href="<?php echo $this->asset ?>index.php">Página Inicial</a></li>
<li><a href="<?php echo $this->asset ?>news.php">Blog</a></li>
<li><a href="<?php echo $this->asset ?>area_atuacao_view.php">Área de Atuação</a></li>
<li><a href="<?php echo $this->asset ?>equipe.php">Equipe</a></li>
<li><a href="<?php echo $this->asset ?>contato.php">Contato</a></li>
<li><a href="#"><span class="glyphicon glyphicon-user"></span> Login</a></li>
</ul>
</nav>
</div>
</nav>
</div>
<!-- /Navigation -->
<!----------------------------------------------------------------------------->
<div class="container-fluid space-4 p-0">
<div id="myCarousel" class="carousel slide" data-ride="carousel">
<ul class="list-group col-sm-4 p-0" >
<li data-target="#myCarousel" data-slide-to="0" class="list-group-item active"><h4>Lorem ipsum dolor sit amet consetetur sadipscing</h4></li>
<li data-target="#myCarousel" data-slide-to="1" class="list-group-item"><h4>consetetur sadipscing elitr, sed diam nonumy eirmod</h4></li>
<li data-target="#myCarousel" data-slide-to="2" class="list-group-item"><h4>consetetur sadipscing elitr, sed diam nonumy eirmod</h4></li>
<li data-target="#myCarousel" data-slide-to="3" class="list-group-item"><h4>consetetur sadipscing elitr, sed diam nonumy eirmod</h4></li>
<li data-target="#myCarousel" data-slide-to="4" class="list-group-item"><h4>consetetur sadipscing elitr, sed diam nonumy eirmod</h4></li>
</ul>
<!-- Wrapper for slides -->
<div class="carousel-inner">

<div class="item active">
<a href="<?php echo $this->asset ?>post_view.php">
<img class="img-responsive" style="margin: 0px auto" src="<?php echo $this->asset ?>img/slide/slide1.jpg" >
</a>
<div class="carousel-caption text-branco">
<h4>
<a href="<?php echo $this->asset ?>post_view.php" class="text-branco">
Lorem ipsum dolor sit amet consetetur sadipscing
</a>
</h4>
<p>
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.
<a class="label label-success" href="<?php echo $this->asset ?>post_view.php">Ler Mais</a>
</p>
</div>
</div><!-- End Item -->
<div class="item">
<a href="<?php echo $this->asset ?>post_view.php">
<img class="img-responsive" style="margin: 0px auto" src="<?php echo $this->asset ?>img/slide/slide02.jpg" >
</a>
<div class="carousel-caption">
<h4><a href="<?php echo $this->asset ?>post_view.php" class="text-branco">consetetur sadipscing elitr, sed diam nonumy eirmod</a></h4>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat. <a class="label label-success" href="<?php echo $this->asset ?>post_view.php">Ler Mais</a></p>
</div>
</div><!-- End Item -->
<div class="item">
<a href="<?php echo $this->asset ?>post_view.php">
<img class="img-responsive" style="margin: 0px auto" src="<?php echo $this->asset ?>http://placehold.it/899x420/dddddd/333333" >
</a>
<div class="carousel-caption">
<h4><a href="<?php echo $this->asset ?>post_view.php" class="text-branco">consetetur sadipscing elitr, sed diam nonumy eirmod</a></h4>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat. <a class="label label-success" href="<?php echo $this->asset ?>post_view.php">Ler Mais</a></p>
</div>
</div><!-- End Item -->
<div class="item">
<a href="<?php echo $this->asset ?>post_view.php">
<img class="img-responsive" style="margin: 0px auto" src="<?php echo $this->asset ?>http://placehold.it/899x420/cccccc/ffffff" >
</a>
<div class="carousel-caption">
<h4><a href="<?php echo $this->asset ?>post_view.php" class="text-branco">consetetur sadipscing elitr, sed diam nonumy eirmod</a></h4>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat. <a class="label label-success" href="<?php echo $this->asset ?>post_view.php">Ler Mais</a></p>
</div>
</div><!-- End Item -->
<div class="item">
<a href="<?php echo $this->asset ?>post_view.php">
<img class="img-responsive" style="margin: 0px auto" src="<?php echo $this->asset ?>http://placehold.it/899x420/999999/cccccc" >
</a>
<div class="carousel-caption">
<h4><a href="<?php echo $this->asset ?>post_view.php" class="text-branco">consetetur sadipscing elitr, sed diam nonumy eirmod</a></h4>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat. <a class="label label-success" href="<?php echo $this->asset ?>post_view.php">Ler Mais</a></p>
</div>
</div><!-- End Item -->

</div><!-- End Carousel Inner -->
<!-- Controls -->
<div class="carousel-controls">
<a class="left carousel-control" href="#myCarousel" data-slide="prev">
<span class="glyphicon glyphicon-chevron-left"></span>
</a>
<a class="right carousel-control" href="#myCarousel" data-slide="next">
<span class="glyphicon glyphicon-chevron-right"></span>
</a>
</div>

</div><!-- End Carousel -->
</div>
</header>
<!-- Quem Somos -->
<section id="about" class="about">
<div class="container wow fadeInUp animated" data-wow-delay="0,5s" data-wow-duration="0,5s">
<div class="row">
<div class="col-lg-12 text-center">
<h2 class="titles-text-cinza">Ambiensense</h2>
<div class="board">
<!-- <h2>Welcome to IGHALO!<sup>™</sup></h2>-->
<div class="board-inner">
<ul class="nav nav-tabs" id="myTab">
<div class="liner"></div>
<li class="active">
<a href="#quemsomos" data-toggle="tab" title="Quem Somos">
<span class="round-tabs one">
<i class="fa fa-group"></i>
</span>
</a>
</li>
<li><a href="#missao" data-toggle="tab" title="Missão">
<span class="round-tabs two">
<i class="fa fa-flag"></i>
</span>
</a>
</li>
<li><a href="#valores" data-toggle="tab" title="Valores">
<span class="round-tabs three">
<i class="fa fa-handshake-o"></i>
</span> </a>
</li>

<li><a href="#visao" data-toggle="tab" title="Visão">
<span class="round-tabs four">
<i class="fa fa-eye"></i>
</span>
</a></li>
</ul></div>

<div class="tab-content">
<div class="tab-pane fade in active" id="quemsomos">

<h3 class="head text-center">Quem Somos</h3>
<p class="narrow text-justify">
<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit. Pellentesque in ipsum id orci porta dapibus.</p>

<p>Donec rutrum congue leo eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat. Donec rutrum congue leo eget malesuada.</p>

<p>Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.</p>
</div>
<div class="tab-pane fade" id="missao">
<h3 class="head text-center">Missão</h3>
<p class="narrow text-justify">
<p>Proin eget tortor risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Proin eget tortor risus.</p>

<p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>
</div>
<div class="tab-pane fade" id="valores">
<h3 class="head text-center">Valores</h3>
<p class="narrow text-justify">
<p>Pellentesque in ipsum id orci porta dapibus. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt.</p>

<p>Pellentesque in ipsum id orci porta dapibus. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Donec rutrum congue leo eget malesuada.</p>
</div>
<div class="tab-pane fade" id="visao">
<h3 class="head text-center">Visão</h3>
<p class="narrow text-justify">
<p>Curabitur aliquet quam id dui posuere blandit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.</p>

<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Quisque velit nisi, pretium ut lacinia in, elementum id enim.</p>
</div>
<div class="clearfix"></div>
</div>

</div>
</div>
</div>
</div>
<!-- /.row -->
<!-- /.container -->
</section>
<!-- Área de atuação -->
<!-- The circle icons use Font Awesome's stacked icon classes. For more information, visit http://fontawesome.io/examples/ -->
<section id="services" class="services bg-a-atuacao">
<div class="container wow fadeInUp animated" data-wow-delay="0,5s" data-wow-duration="0,5s">
<div class="row text-center">
<div class="col-lg-12">
<h2 class="titles-text-branco">Áreas de atuação</h2>
<hr class="small">
<div class="row">
<!-- ↓↓↓ ISTO TUDO VAI PARA DENTRO DE UMA VARIAVEL PHP E SERÁ LISTADA NO FOREACH ↓↓↓↓ -->
<?php foreach ($data['atuacao'] as $area_atuacao): ?>
<!----------------------------------------------------------------------------->
<div class="col-md-4 col-md-4 col-sm-6 wow m-auto fadeInUp animated" data-wow-delay="0,5s" data-wow-duration="0,5s">
<div class="panel panel-default">
<div class="panel-heading">
<h4>
<!--↓↓↓  AQUI VAI O NOME DA ÁREA DE ATUAÇÃO ↓↓↓↓-->
<strong class="text-verde">titulo -> <?php echo $area_atuacao->getNome() ?></strong>
<!-- ↑↑↑  AQUI VAI O NOME DA ÁREA DE ATUAÇÃO ↑↑↑-->
</h4>
</div>
<div class="panel-body">

<!--↓↓↓  AQUI VAI O NOME DA IMAGEM DA ÁREA DE ATUAÇÃO ↓↓↓↓-->
<img src="<?php echo $this->base_url?>system/upload/<?php echo $area_atuacao->getImagem_atuacao()?>" class="img-responsive" style="margin: 0px auto;" alt="$THIS->NOME AREA DE ATUAÇAO" />
<!-- ↑↑↑  AQUI VAI O NOME DA IMAGEM DA ÁREA DE ATUAÇÃO ↑↑↑-->
<!--↓↓↓  AQUI VAI A DESCRIÇÃO DA ÁREA DE ATUAÇÃO ↓↓↓↓-->
<p class="text-default">
descri1 - ><?php echo $area_atuacao->getDescricao_curta() ?>
</p>
<!--↓↓↓  AQUI VAI O CODIGO DA ÁREA DE ATUAÇÃO OU O QUE SERA PASSADO PELO LINK↓↓↓↓-->
<a href="<?php echo $this->base_url ?>List_area_de_atuacao/view_area_de_atuacao/<?php echo $area_atuacao->getId_atuacao() ?>" class=" btn btn-success col-lg-12">
<!--↑↑↑  AQUI VAI O CODIGO DA ÁREA DE ATUAÇÃO OU O QUE SERA PASSADO PELO LINK ↑↑↑-->
<i class="fa fa-plus"></i>
Ler Mais
</a>
</div>
</div>
</div>
<?php endforeach ?>
<!-- ↑↑↑ ISSO VAI PARA A VARIAVEL PHP PARA LISTAR NO FOREACH DA AREA DE ATUAÇÃO↑↑↑-->
<!---------------------------------------------------------------------------->
</div>
</div>
<!----------------------------------------------------------------------------->
<!-- /.row (nested) -->
</div>
<!-- /.col-lg-10 -->
</div>
<!-- /.row -->
</div>
<!-- /.container -->
</section>
<!-- Portfolio -->
<section id="portfolio" class="portfolio">
<div class="container text-center">
<h2 class="titles-text-cinza">A Equipe</h2>
<hr class="small">
<div class="row wow fadeInUp animated" data-wow-delay="0,5s" data-wow-duration="0,5s">
<!--↓↓↓ ISSO VAI PARA A VARIAVEL PHP NUM FOREACH PARA LISTAR A EQUIPE ↓↓↓-->
<?php foreach ($data['equipes'] as $equipe): ?>
<!----------------------------------------------------------------------------->
<div class="col-lg-4 col-md-4 col-sm-4">

<!--↓↓↓  AQUI VAI O NOME DA IMAGEM DA PESSOA DA EQUPE↓↓↓↓-->
<img class="img-circle" src="<?php echo $this->base_url?>system/upload/<?php echo $news->getImagem_equipe()?>" alt="Generic placeholder image" width="140" height="140">
<!--↑↑↑  AQUI VAI O NOME DA IMAGEM DA PESSOA DA EQUIPE ↑↑↑-->
<!--↓↓↓  AQUI VAI O NOME DA PESSOA DA EQUPE↓↓↓↓-->
<h2>nome -> <?php echo $equipe->getNome() ?></h2>
<!--↓↓↓  AQUI VAI A DESCRIÇÃO DA PESSOA DA EQUPE↓↓↓↓-->
<p>descrição - ><?php echo $equipe->getFuncao() ?></p>
<!--↑↑↑  AQUI VAI A DESCRIÇÃO DA PESSOA DA EQUIPE ↑↑↑-->
<!--↓↓↓  AQUI VAI O LINK PARA A PAGINA DA PESSOA DA EQUPE↓↓↓↓-->
<p><a class="btn btn-secondary" href="<?php echo $this->base_url ?>List_equipe/view_equipe/<?php echo $equipe->getId_equipe() ?>" role="button">View details &raquo;</a></p>
<!--↑↑↑  AQUI VAI O LINK PARA A PAGINA DA PESSOA DA EQUIPE ↑↑↑-->
</div>
<?php endforeach ?>
<!----------------------------------------------------------------------------->
<!--↑↑↑ ISSO VAI PARA A VARIAVEL PHP NUM FOREACH PARA LISTAR A EQUIPE ↑↑↑-->
</div>
<!-- /.row -->
</div>
<!-- /.container -->
</section>
<!-- Call to Action -->
<section id="parallaxBar" data-speed="10" data-type="background" class="objetivos-milenio bg-obj">
<div class="container">
<h2 class="titles-text-branco text-center">Objetivos de Desenvolvimento do Milênio</h2>
<hr class="small">
<div class="row wow fadeInUp animated" data-wow-delay="0.5s" data-wow-duration="1s">
<!---------------------- Objetivos do Milenio ------------------------------------------------------>
<!---------------------- Objetivo 1 ------------------------------------------------------>
<div class="col-lg-3 col-md-6 col-sm-6 wow animated bounceIn" data-wow-delay="0.5s">
<div class="grid cs-style-4 hv-shadow">
<div>
<figure>
<div><img src="<?php echo $this->asset ?>img/obj-milenio/obj1.png" class="img-responsive" alt="Objetivo 1"></div>
<figcaption class="obj1">
<div style="height: 100%; padding: 2px">
<h3>Objetivo 1</h3>
<p>

Reduzir pela metade, até 2015, a proporção da população com renda inferior a um dólar por dia e a proporção da população que sofre de fome.
</p>
</div>
</figcaption>
</figure>
</div>
</div>
</div>
<!---------------------- Objetivo 2 ------------------------------------------------------>
<div class="col-lg-3 col-md-6 col-sm-6 wow animated bounceIn" data-wow-delay="0.7s">
<div class="grid cs-style-4 hv-shadow">
<div>
<figure>
<div><img src="<?php echo $this->asset ?>img/obj-milenio/obj2.png" class="img-responsive" alt="Objetivo 2"></div>
<figcaption class="obj2">
<div style="height: 100%; padding: 2px">
<h3>Objetivo 2</h3>
<p>
Garantir que, até 2015, todas as crianças, de ambos os sexos, tenham recebido educação de qualidade e concluído o ensino básico.
</p>
</div>
</figcaption>
</figure>
</div>
</div>
</div>
<!---------------------- Objetivo 3 ------------------------------------------------------>
<div class="col-lg-3 col-md-6 col-sm-6 wow animated bounceIn" data-wow-delay="1s">
<div class="grid cs-style-4 hv-shadow">
<div>
<figure>
<div><img src="<?php echo $this->asset ?>img/obj-milenio/obj3.png" class="img-responsive" alt="Objetivo 3"></div>
<figcaption class="obj3">
<div style="height: 100%; padding: 2px">
<h3>Objetivo 3</h3>
<p>
Eliminar a disparidade entre os sexos no ensino em todos os níveis de ensino, no mais tardar até 2015.
</p>
</div>
</figcaption>
</figure>
</div>
</div>
</div>
<!---------------------- Objetivo 4 ------------------------------------------------------>
<div class="col-lg-3 col-md-6 col-sm-6 wow animated bounceIn" data-wow-delay="1.2s">
<div class="grid cs-style-4 hv-shadow">
<div>
<figure>
<div><img src="<?php echo $this->asset ?>img/obj-milenio/obj4.png" class="img-responsive" alt="Objetivo 4"></div>
<figcaption class="obj4">
<div style="height: 100%; padding: 2px">
<h3>Objetivo 4</h3>
<p>
Reduzir em dois terços, até 2015, a mortalidade de crianças menores de 5 anos.
</p>
</div>
</figcaption>
</figure>
</div>
</div>
</div>
<!---------------------- Objetivo 5 ------------------------------------------------------>
<div class="col-lg-3 col-md-6 col-sm-6 wow animated bounceIn" data-wow-delay="1.5s">
<div class="grid cs-style-4 hv-shadow">
<div>
<figure>
<div><img src="<?php echo $this->asset ?>img/obj-milenio/obj5.png" class="img-responsive" alt="Objetivo 5"></div>
<figcaption class="obj5">
<div style="height: 100%; padding: 2px">
<h3>Objetivo 5</h3>
<p>
Reduzir em três quartos, até 2015, a taxa de mortalidade materna. Deter o crescimento da mortalidade por câncer de mama e de colo de útero.
</p>
</div>
</figcaption>
</figure>
</div>
</div>
</div>
<!---------------------- Objetivo 6 ------------------------------------------------------>
<div class="col-lg-3 col-md-6 col-sm-6 wow animated bounceIn" data-wow-delay="1.7s">
<div class="grid cs-style-4 hv-shadow">
<div>
<figure>
<div><img src="<?php echo $this->asset ?>img/obj-milenio/obj6.png" class="img-responsive" alt="Objetivo 6"></div>
<figcaption class="obj6">
<div style="height: 100%; padding: 2px">
<h3>Objetivo 6</h3>
<p>
Até 2015, ter detido a propagação do HIV/Aids e garantido o acesso universal ao tratamento. Deter a incidência da malária, da tuberculose e eliminar a hanseníase.
</p>
</div>
</figcaption>
</figure>
</div>
</div>
</div>
<!---------------------- Objetivo 7 ------------------------------------------------------>
<div class="col-lg-3 col-md-6 col-sm-6 wow animated bounceIn" data-wow-delay="1.9s">
<div class="grid cs-style-4 hv-shadow">
<div>
<figure>
<div><img src="<?php echo $this->asset ?>img/obj-milenio/obj7.png" class="img-responsive" alt="Objetivo 7"></div>
<figcaption class="obj7">
<div style="height: 100%; padding: 2px">
<h3>Objetivo 7</h3>
<p>
Promover o desenvolvimento sustentável, reduzir a perda de diversidade biológica e reduzir pela metade, até 2015, a proporção da população sem acesso a água potável e esgotamento sanitário.
</p>
</div>
</figcaption>
</figure>
</div>
</div>
</div>
<!---------------------- Objetivo 8 ------------------------------------------------------>
<div class="col-lg-3 col-md-6 col-sm-6 wow animated bounceIn" data-wow-delay="2.1s">
<div class="grid cs-style-4 hv-shadow">
<div>
<figure>
<div><img src="<?php echo $this->asset ?>img/obj-milenio/obj8.png" class="img-responsive" alt="Objetivo 8"></div>
<figcaption class="obj8">
<div style="height: 100%; padding: 2px">
<h3>Objetivo 8</h3>
<p>
Avançar no desenvolvimento de um sistema comercial e financeiro não discriminatório. Tratar globalmente o problema da dívida dos países em desenvolvimento. Formular e executar estratégias que ofereçam aos jovens um trabalho digno e produtivo. Tornar acessíveis os benefícios das novas tecnologias, em especial de informação e de comunicações.
</p>
</div>
</figcaption>
</figure>
</div>
</div>
</div>
<!----------------------------------------------------------------------------->
</div>
</div>
</section>
<!-- ↓↓↓ AQUI VAI O INPUT PARA O ÚSUARIO PASSAR O EMAIL ↓↓↓ -->
<section id="contact" >
<div class="container">
<div class="row text-center">
<div class="col-lg-12">
<h2 class="titles-text-cinza">Quer receber nossos conteudos?</h2>
<hr class="small">
<div class="col-lg-4 col-md-4 col-sm-4"></div>
<div class="col-lg-4 col-md-4 col-sm-4">
<form class="form-search">
<div class="input-group">
<input type="email" class="form-control " placeholder="digite seu e-mail">
<span class="input-group-btn">
<button type="submit" class="btn btn-search">Enviar</button>
</span>
</div>
</form>
</div>
<div class="col-lg-4 col-md-4 col-sm-4"></div>
</div>
</div>
</div>
</section>
<!-- ↑↑↑ AQUI VAI O INPUT PARA O ÚSUARIO PASSAR O EMAIL ↑↑↑ -->
<!-- Footer -->
<?php include ('./rodape.php'); ?>