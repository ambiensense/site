particlesJS('particles-js',
{
"particles": {
"number": {
"value": 110,
"density": {
"enable": true,
"value_area": 300
}
},
"color": {
"value": "#f90909"
},
"shape": {
"type": "circle",
"stroke": {
"width": 0,
"color": "#f90909"
},
"Circle": {
"nb_sides": 5
},
"image": {
"src": "img/github.svg",
"width": 100,
"height": 100
}
},
"opacity": {
"value": 1,
"random": false,
"anim": {
"enable": false,
"speed": 1,
"opacity_min": 0.1,
"sync": false
}
},
"size": {
"value": 3,
"random": true,
"anim": {
"enable": false,
"speed": 40,
"size_min": 0.5,
"sync": false
}
},
"line_linked": {
"enable": true,
"distance": 150,
"color": "#f90909",
"opacity": 0.6,
"width": 1
},
"move": {
"enable": true,
"speed": 3,
"direction": "none",
"random": false,
"straight": false,
"out_mode": "out",
"bounce": false,
"attract": {
"enable": false,
"rotateX": 600,
"rotateY": 1200
}
}
},
"interactivity": {
"detect_on": "window",
"events": {
"onhover": {
"enable": true,
"mode": "bubble"
},
"onclick": {
"enable": false,
"mode": "push"
},
"resize": true
},
"modes": {
"grab": {
"distance": 12.181158184520179,
"line_linked": {
"opacity": 1
}
},
"bubble": {
"distance": 36.54347455356053,
"size": 24.362316369040354,
"duration": 0.9744926547616141,
"opacity": 0.3167101127975246,
"speed": 3
},
"repulse": {
"distance": 113.69080972218832,
"duration": 0.4
},
"push": {
"particles_nb": 4
},
"remove": {
"particles_nb": 2
}
}
},
"retina_detect": true
}

);